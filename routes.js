const router = require('express').Router();

const ordersController = require('./controllers/orders.controller.js');

router.use('/orders', ordersController);

// GET / - default
router.get('/', (req, res) => {

  res.json({
    title: 'Broce Parts API'
  });
  
});

module.exports = router;